FROM openjdk:17-slim
WORKDIR /app
COPY target/*.jar /app/warranty-gateway.jar
EXPOSE 9080:9080
CMD ["java", "-jar", "warranty-gateway.jar"]
